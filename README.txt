********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Massdelete Module
Author: John VanDyk <jvandyk at iastate dot edu>
Drupal: 5.x
********************************************************************
DESCRIPTION:

A very dangerous module. Use with extreme care, and only after
making a backup of your database.

Useful for, e.g. a college course in Drupal where you need to "reset"
the course for a new semester.

This module deletes all nodes except nodes belonging to users you 
select.

After enabling this module, the process is:

1. Click on Administer -> Content management -> Delete all content.
2. Select users that you wish to preserve (nodes owned by them
   will be preserved too).
3. Proceed to delete all other nodes, and all other users.

********************************************************************
